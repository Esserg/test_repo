#!/bin/bash

if [ "$(whoami)" != "root" ]; then
	echo "Run script as ROOT please. (sudo !!)"
	exit
fi

apt-get update -y
apt-get upgrade -y
apt-get dist-upgrade -y

apt-get install -y rpi-update

apt-get install -y php7.2 php7.2-fpm php7.2-cli php7.2-opcache php7.2-mbstring php7.2-curl php7.2-xml php7.2-gd php7.2-mysql
apt-get install -y nginx


update-rc.d nginx defaults
update-rc.d php7.2-fpm defaults

sed -i 's/^;cgi.fix_pathinfo=1/cgi.fix_pathinfo=0/' /etc/php/7.2/fpm/php.ini
sed -i 's/# server_names_hash_bucket_size/server_names_hash_bucket_size/' /etc/nginx/nginx.conf
